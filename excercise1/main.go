/*package main

import (
	"bufio"
	"flag"
	"fmt"
	"gophercises/excercise1/src"
	"log"
	"os"
	"strings"
)

func main() {
	e, u, err := loadResources()
	if err != nil {
		log.Fatal("Not able load the file", err)
	}
	startExam(e, u)
}

func startExam(e src.ExamPaper, u src.User) {
	for {
		err, question := e.NextQuestion()
		if err != nil {
			fmt.Println("End of the Exam, Posting the scores soon")
			fmt.Print("User Score is:", u.GetScore())
			return
		}
		fmt.Println("Question:", strings.Split(question, ",")[0])
		_, userAnswer := u.AnswerQuestion()
		e.CheckQuestionIsCorrect(strings.TrimSpace(userAnswer), strings.TrimSpace(strings.Split(question, ",")[1]), &u)
	}
}

func loadResources() (src.ExamPaper, src.User, error) {
	var path = flag.String("path", "resources/problems.csv", "Question paper")
	e := src.ExamPaper{10, nil}
	u := src.User{Name: "Vasu", Reader: bufio.NewReader(os.Stdin)}
	err := e.LoadExamPaper(*path)
	f := src.ExamPaper{}
	fmt.Println("current index is:", f.GetCurrentIndex())
	return e, u, err
}
*/

package main

import (
	"encoding/json"
	"fmt"
	"gophercises/excercise1/src"
	"time"
)

func main() {
	src.InitialiseRedisCluster()
	val, _ := json.Marshal(src.KeycloakIntrospectResponse{
		Jti:               "",
		Exp:               0,
		Nbf:               0,
		Iat:               0,
		Iss:               "",
		Aud:               "",
		Sub:               "",
		Typ:               "",
		Azp:               "",
		AuthTime:          0,
		SessionState:      "",
		Name:              "",
		GivenName:         "",
		FamilyName:        "",
		PreferredUsername: "",
		Email:             "",
		Acr:               "",
		AllowedOrigins:    nil,
		RealmAccess:       src.RealmAccess{},
		ClientID:          "",
		UserName:          "",
		Active:            true,
	})
	src.SetKeyValuePair("token", val, 10*time.Minute)

	v, _ := src.GetValueFromKey("token12")
	if v != nil && v != "" {
		fmt.Print(v)

	} else {
		fmt.Print("no key available")
	}
}
