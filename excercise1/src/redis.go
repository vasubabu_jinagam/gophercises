package src

import (
	"fmt"
	"github.com/go-redis/redis"
	"sync"
	"time"
)

var (
	RedisInstance *redis.Client
	once          sync.Once
)

// init ensures that only a single instance of the
// struct singleton gets created and provides a global point
// to access to it.
func InitialiseRedisCluster() {
	once.Do(func() {
		RedisInstance = redis.NewClient(&redis.Options{
			Addr: "localhost:6379",
			DB:   0,
		})
		_, err := RedisInstance.Ping().Result()
		if err != nil {
		}
	})

}

func GetValueFromKey(key string) (interface{}, error) {
	val, err := RedisInstance.Get(key).Result()
	if err == redis.Nil {
		return nil, err
	} else if err != nil {
	}
	return val, nil
}

//store <key,value> pair in redis cluster
func SetKeyValuePair(key string, val []byte, TTL time.Duration) {
	err := RedisInstance.Set(key, val, TTL).Err()
	if err != nil {
		fmt.Print(err)
	}
}

func DelKey(key string) {
	RedisInstance.Del(key)
}
