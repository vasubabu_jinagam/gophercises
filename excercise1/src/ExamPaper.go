package src

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
)

type ExamPaper struct {
	CurrentIndex int32 // last appeared Question number
	Reader      *csv.Reader
}

func (e *ExamPaper) LoadExamPaper(filePath string) error {
	csvfile, err := os.Open(filePath)

	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
		return err
	}

	e.Reader = csv.NewReader(csvfile)
	return nil
}

func (e *ExamPaper) NextQuestion() (error, string) {
	record, err := e.Reader.Read()
	e.CurrentIndex++;
	if err == io.EOF {
		return errors.New("end of the paper"), ""
	}
	if err != nil {
		log.Fatal(err)
	}
	return nil, record[0] + "," + record[1]
}

func (e *ExamPaper) GetCurrentIndex() int32 {
	return e.CurrentIndex
}

func (e *ExamPaper) CheckQuestionIsCorrect(userAnswer string, correctAnswer string, u *User ) {
	if correctAnswer == userAnswer {
		u.Increment()
	} else {
		fmt.Println("Sorry!!! Its a wrong answer")
	}
}
