package src

import (
	"bufio"
	"fmt"
)

type User struct {
	Name           string
	CorrectAnswers int32
	Reader         *bufio.Reader
	Score int32
}

func (u *User) AnswerQuestion() (error, string) {
	answer, _ := u.Reader.ReadString('\n')
	return nil, answer
}


func (u *User) Increment() {
	fmt.Println("Right Answer!!!")
	u.Score++
}

func (u *User) GetScore() (int32){
	return u.Score
}