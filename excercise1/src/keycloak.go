package src

type KeycloakIntrospectResponse struct {
	Jti               string      `json:"jti"`
	Exp               int64       `json:"exp"`
	Nbf               int64       `json:"nbf"`
	Iat               int64       `json:"iat"`
	Iss               string      `json:"iss"`
	Aud               string      `json:"aud"`
	Sub               string      `json:"sub"`
	Typ               string      `json:"typ"`
	Azp               string      `json:"azp"`
	AuthTime          int64       `json:"auth_time"`
	SessionState      string      `json:"session_state"`
	Name              string      `json:"name"`
	GivenName         string      `json:"given_name"`
	FamilyName        string      `json:"family_name"`
	PreferredUsername string      `json:"preferred_username"`
	Email             string      `json:"email"`
	Acr               string      `json:"acr"`
	AllowedOrigins    []string    `json:"allowed-origins"`
	RealmAccess       RealmAccess `json:"realm_access"`
	ClientID          string      `json:"client_id"`
	UserName          string      `json:"username"`
	Active            bool        `json:"active"`
}

type RealmAccess struct {
	Roles []string `json:"roles"`
}
