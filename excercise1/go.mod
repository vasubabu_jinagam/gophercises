module gophercises/excercise1

go 1.13

replace github.com/mailgun/mailgun-go => github.com/mailgun/mailgun-go v0.0.0-20180718145431-bbe57f98d550

replace github.com/census-instrumentation/opencensus-proto v0.1.0-0.20181214143942-ba49f56771b8 => github.com/census-instrumentation/opencensus-proto v0.0.3-0.20181214143942-ba49f56771b8

replace github.com/lucas-clemente/quic-go => github.com/lucas-clemente/quic-go v0.12.1

require (
	bitbucket.org/sixtgoorange/com.sixt.service.mydriver-user-manager v1.0.2
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/pkg/errors v0.8.1
)
