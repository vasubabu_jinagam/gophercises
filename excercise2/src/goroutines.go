package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	c := fanIn(speakVasu("vasu"), speakHari("hari"))
	for i:=1 ; i<10; i++ {
		fmt.Println(<-c)
	}
	fmt.Println("You are very boring, leaving")
}

func speakVasu(msg string) <- chan string {
	channel := make(chan string)
	go func() {
		for i:=0; ; i++ {
			time.Sleep(1*time.Millisecond)
			channel <- fmt.Sprintf("%s - %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()
	return channel
}

func speakHari(msg string) <- chan string {
	channel := make(chan string)
	go func() {
		for i:=0; ; i++ {
			channel <- fmt.Sprintf("%s - %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()
	return channel
}

func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() { for { c <- <-input1}}()
	go func() { for { c <- <-input2}}()
	return c
}